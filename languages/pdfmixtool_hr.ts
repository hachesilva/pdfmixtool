<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hr">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>Informacije o PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Zatvori</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>Verzija %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="71"/>
        <source>Website</source>
        <translation>Web-stranica</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="78"/>
        <source>About</source>
        <translation>Informacije</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="68"/>
        <source>An application to perform common editing operations on PDF files.</source>
        <translation>Program za izvođenje uobičajenih operacija uređivanja PDF datoteka.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>Prevodioci</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>Zasluge</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>Licenca</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>Pošalji zahtjev za povlačenje</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>Prijavi grešku</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>Pomogni prevoditi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>Doprinesi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>Zapis promjena</translation>
    </message>
</context>
<context>
    <name>AbstractOperation</name>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="36"/>
        <source>Save</source>
        <translation>Spremi</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="64"/>
        <source>Overwrite File?</source>
        <translation>Prepisati datoteku?</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="65"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>Datoteka s imenom „%1” već postoji. Želiš li je prepisati?</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="71"/>
        <source>Always overwrite</source>
        <translation>Uvijek prepiši</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="89"/>
        <source>Save PDF file</source>
        <translation>Spremi PDF datoteku</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="93"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF datoteke (*.pdf)</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="34"/>
        <source>Add empty pages</source>
        <translation>Dodaj prazne stranice</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="46"/>
        <source>Count:</source>
        <translation>Broj:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="50"/>
        <source>Page size</source>
        <translation>Veličina stranice</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="53"/>
        <source>Same as document</source>
        <translation>Ista kao i dokument</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="56"/>
        <source>Custom:</source>
        <translation>Prilagođeno:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="81"/>
        <source>Standard:</source>
        <translation>Standardno:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="90"/>
        <source>Portrait</source>
        <translation>Uspravno</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="92"/>
        <source>Landscape</source>
        <translation>Polegnuto</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="95"/>
        <source>Location</source>
        <translation>Položaj</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="98"/>
        <source>Before</source>
        <translation>Ispred</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="100"/>
        <source>After</source>
        <translation>Iza</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="103"/>
        <source>Page:</source>
        <translation>Stranica:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="117"/>
        <source>Save as…</source>
        <translation>Spremi kao …</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="33"/>
        <source>Booklet</source>
        <translation>Knjižica</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="42"/>
        <source>Left</source>
        <translation>Lijevo</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="43"/>
        <source>Right</source>
        <translation>Desno</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="44"/>
        <source>Binding:</source>
        <translation>Uvez:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="45"/>
        <source>Use last page as back cover:</source>
        <translation>Koristi zadnju stranicu kao stražnje korice:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="52"/>
        <source>Generate booklet</source>
        <translation>Izradi knjižicu</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="68"/>
        <source>Save booklet PDF file</source>
        <translation>Spremi PDF datoteku knjižice</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="72"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF datoteke (*.pdf)</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="34"/>
        <source>Delete pages</source>
        <translation>Ukloni stranice</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="54"/>
        <source>Save as…</source>
        <translation>Spremi kao …</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Uredi višestranični profil</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="39"/>
        <source>Standard size:</source>
        <translation>Standardna veličina:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="40"/>
        <source>Width:</source>
        <translation>Širina:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="41"/>
        <source>Height:</source>
        <translation>Visina:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>Lijevo</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>Sredina</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>Desno</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>Gore</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>Dolje</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>Ime:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>Izlazna veličina stranice</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>Prilagođena veličina:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>Raspored stranica</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>Broj redaka:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>Broj stupaca:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>Okretanje:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>Razmak:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>Poravnanje stranica</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>Vodoravno:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>Okomito:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>Margine</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="34"/>
        <source>Edit page layout</source>
        <translation>Uredi prijelom stranice</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="55"/>
        <source>No rotation</source>
        <translation>Bez okretanja</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="62"/>
        <source>Rotation:</source>
        <translation>Okretanje:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="64"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="124"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="211"/>
        <source>Disabled</source>
        <translation>Deaktivirano</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="72"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="136"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="219"/>
        <source>New custom profile…</source>
        <translation>Novi prilagođeni profil …</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="78"/>
        <source>Multipage:</source>
        <translation>Višestranično:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="86"/>
        <source>Scale page:</source>
        <translation>Skaliraj stranicu:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="94"/>
        <source>Save as…</source>
        <translation>Spremi kao …</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Uredi svojstva PDF datoteke</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Bez okretanja</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Deaktivirano</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Odustani</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Višestranično:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Okretanje:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="53"/>
        <source>Output PDF base name:</source>
        <translation>Osnovno ime PDF rezultata:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="49"/>
        <source>Extract to individual PDF files</source>
        <translation>Izdvoji u pojedinačne PDF datoteke</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="37"/>
        <source>Extract pages</source>
        <translation>Izdvoji stranice</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="62"/>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="84"/>
        <source>Extract…</source>
        <translation>Izdvoji …</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="79"/>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="190"/>
        <source>Extract to single PDF</source>
        <translation>Izdvoji u pojedinačni PDF</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="114"/>
        <source>Select save directory</source>
        <translation>Odaberi mapu za spremanje</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="194"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF datoteke (*.pdf)</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="113"/>
        <source>All</source>
        <translation>Sve</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="80"/>
        <source>Page order:</source>
        <translation>Redoslijed stranica:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="83"/>
        <source>reverse</source>
        <translation>obrnuti</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="85"/>
        <source>forward</source>
        <translation>normalni</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <source>Pages:</source>
        <translation>Stranice:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="129"/>
        <source>Multipage:</source>
        <translation>Višestranično:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="134"/>
        <source>Disabled</source>
        <translation>Deaktivirano</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="137"/>
        <source>Rotation:</source>
        <translation>Okretanje:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="140"/>
        <source>Outline entry:</source>
        <translation>Unos strukture:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="57"/>
        <source>Reverse page order:</source>
        <translation>Obrnut redoslijed stranica:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="71"/>
        <source>Disabled</source>
        <translation>Deaktivirano</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="79"/>
        <source>New custom profile…</source>
        <translation>Novi prilagođeni profil …</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="81"/>
        <source>No rotation</source>
        <translation>Bez okretanja</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="86"/>
        <source>Pages:</source>
        <translation>Stranice:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="88"/>
        <source>Multipage:</source>
        <translation>Višestranično:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="90"/>
        <source>Rotation:</source>
        <translation>Okretanje:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="92"/>
        <source>Outline entry:</source>
        <translation>Unos strukture:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="175"/>
        <source>Add PDF file</source>
        <translation>Dodaj PDF datoteku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="187"/>
        <source>Move up</source>
        <translation>Premjesti gore</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="192"/>
        <source>Move down</source>
        <translation>Premjesti dolje</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>Remove file</source>
        <translation>Ukloni datoteku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="128"/>
        <source>About</source>
        <translation>Informacije</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="248"/>
        <location filename="../src/mainwindow.cpp" line="252"/>
        <source>Generate PDF</source>
        <translation>Izradi PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="473"/>
        <source>Select the JSON file containing the files list</source>
        <translation>Odaberi JSON datoteku koja sadrži popis datoteka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="853"/>
        <source>PDF generation error</source>
        <translation>Greška tijekom izrade PDF-a</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="180"/>
        <source>Select one or more PDF files to open</source>
        <translation>Odaberi i otvori jednu ili više PDF datoteka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Edit</source>
        <translation>Uredi</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="165"/>
        <source>View</source>
        <translation>Pogledaj</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <source>Main toolbar</source>
        <translation>Glavna alatna traka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>Menu</source>
        <translation>Izbornik</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="95"/>
        <source>Multiple files</source>
        <translation>Višestruke datoteke</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="96"/>
        <source>Single file</source>
        <translation>Pojedinačna datoteka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="123"/>
        <source>Multipage profiles…</source>
        <translation>Višestranični profili …</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="133"/>
        <source>Exit</source>
        <translation>Izlaz</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="208"/>
        <source>Load files list</source>
        <translation>Učitaj popis datoteka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="213"/>
        <source>Save files list</source>
        <translation>Spremi popis datoteka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Open PDF file…</source>
        <translation>Otvori PDF datoteku …</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <location filename="../src/mainwindow.cpp" line="952"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF datoteke (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="48"/>
        <source>Alternate mix</source>
        <translation>Naizmjenično miješanje</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="475"/>
        <location filename="../src/mainwindow.cpp" line="493"/>
        <source>JSON files (*.json)</source>
        <translation>JSON datoteke (*.json)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="481"/>
        <source>Error while reading the JSON file!</source>
        <translation>Greška tijekom čitanja JSON datoteke!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="482"/>
        <source>An error occurred while reading the JSON file!</source>
        <translation>Došlo je do greške tijekom čitanja JSON datoteke!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="489"/>
        <source>Select a JSON file</source>
        <translation>Odaberi JSON datoteku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="602"/>
        <location filename="../src/mainwindow.cpp" line="971"/>
        <source>Error opening file</source>
        <translation>Greška tijekom otvaranja datoteke</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <source>Output pages: %1</source>
        <translation>Broj izlaznih stranica: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="950"/>
        <source>Select a PDF file</source>
        <translation>Odaberi PDF datoteku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Files saved in %1.</source>
        <translation>Datoteke su spremljene u %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>File %1 saved.</source>
        <translation>Datoteka %1 je spremljena.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="836"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Izlazne stranice datoteke &lt;b&gt;%1&lt;/b&gt; su loše formatirane. Obavezno se drži sljedećih pravila:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;rasponi stranica moraju se zadati s prvom i zadnjom stranicom, odvojene crticom (npr. „1-5”);&lt;/li&gt;&lt;li&gt;pojedinačne stranice i rasponi stranica moraju se odvojiti s razmacima, sa zarezima ili s razmacima i zarezima (npr. „1, 2, 3, 5-10” ili „1 2 3 5-10”);&lt;/li&gt;&lt;li&gt;sve stranice i rasponi stranica moraju biti između 1 i ukupnog broja stranica PDF datoteke;&lt;/li&gt;&lt;li&gt;mogu se koristiti samo brojevi, razmaci, zarezi i crtice. Svi drugi znakovi nisu dozvoljeni.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="860"/>
        <source>Save PDF file</source>
        <translation>Spremi PDF datoteku</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Novi profil …</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="35"/>
        <source>Delete profile</source>
        <translation>Izbriši profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="43"/>
        <source>Manage multipage profiles</source>
        <translation>Upravljaj višestraničnim profilima</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="76"/>
        <source>Edit profile</source>
        <translation>Uredi profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="122"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="124"/>
        <source>Custom profile</source>
        <translation>Prilagođeni profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="183"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="192"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="206"/>
        <source>Error</source>
        <translation>Greška</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="184"/>
        <source>Profile name can not be empty.</source>
        <translation>Ime profila ne smije biti prazno.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <source>Disabled</source>
        <translation>Deaktivirano</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="193"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="207"/>
        <source>Profile name already exists.</source>
        <translation>Ime profila već postoji.</translation>
    </message>
</context>
<context>
    <name>PagesSelector</name>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="44"/>
        <source>Pages:</source>
        <translation>Stranice:</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="45"/>
        <source>Even pages</source>
        <translation>Parne stranice</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="46"/>
        <source>Odd pages</source>
        <translation>Neparne stranice</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="48"/>
        <source>All pages</source>
        <translation>Sve stranice</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="108"/>
        <source>&lt;p&gt;Page intervals are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Rasponi stranica su loše formatirani. Obavezno se drži sljedećih pravila:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;rasponi stranica moraju se zadati s prvom i zadnjom stranicom, odvojene crticom (npr. „1-5”);&lt;/li&gt;&lt;li&gt;pojedinačne stranice i rasponi stranica moraju se odvojiti s razmacima, sa zarezima ili s razmacima i zarezima (npr. „1, 2, 3, 5-10” ili „1 2 3 5-10”);&lt;/li&gt;&lt;li&gt;sve stranice i rasponi stranica moraju biti između 1 i ukupnog broja stranica PDF datoteke;&lt;/li&gt;&lt;li&gt;mogu se koristiti samo brojevi, razmaci, zarezi i crtice. Svi drugi znakovi nisu dozvoljeni.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="124"/>
        <source>Error</source>
        <translation>Greška</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>uspravno</translation>
    </message>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>polegnuto</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/widgets/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n stranica</numerusform>
            <numerusform>%n stranice</numerusform>
            <numerusform>%n stranica</numerusform>
        </translation>
    </message>
</context>
</TS>
