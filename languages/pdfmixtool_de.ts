<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>Über PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="71"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="78"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="68"/>
        <source>An application to perform common editing operations on PDF files.</source>
        <translation>Eine Anwendung zur Durchführung allgemeiner Bearbeitungsvorgänge an PDF-Dateien.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>Übersetzer</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>Beiträge</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>Sende einen Pull-Request</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>Melde einen Fehler</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>Beim Übersetzen helfen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>Beitragen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>Änderungsprotokoll</translation>
    </message>
</context>
<context>
    <name>AbstractOperation</name>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="36"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="64"/>
        <source>Overwrite File?</source>
        <translation>Datei überschreiben?</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="65"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>Eine Datei namens «%1» existiert bereits. Wollen Sie diese überschreiben?</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="71"/>
        <source>Always overwrite</source>
        <translation>Immer überschreiben</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="89"/>
        <source>Save PDF file</source>
        <translation>PDF-Datei speichern</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/abstract_operation.cpp" line="93"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF-Dateien (*.pdf)</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="34"/>
        <source>Add empty pages</source>
        <translation>Leere Seiten hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="46"/>
        <source>Count:</source>
        <translation>Anzahl:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="50"/>
        <source>Page size</source>
        <translation>Seitengröße</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="53"/>
        <source>Same as document</source>
        <translation>Gleich wie das Dokument</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="56"/>
        <source>Custom:</source>
        <translation>Eigenes:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="81"/>
        <source>Standard:</source>
        <translation>Standard:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="90"/>
        <source>Portrait</source>
        <translation>Hochformat</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="92"/>
        <source>Landscape</source>
        <translation>Querformat</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="95"/>
        <source>Location</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="98"/>
        <source>Before</source>
        <translation>Vor</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="100"/>
        <source>After</source>
        <translation>Nach</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="103"/>
        <source>Page:</source>
        <translation>Seite:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/add_empty_pages.cpp" line="117"/>
        <source>Save as…</source>
        <translation>Speichern unter…</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="33"/>
        <source>Booklet</source>
        <translation>Broschüre</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="42"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="43"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="44"/>
        <source>Binding:</source>
        <translation>Bindung:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="45"/>
        <source>Use last page as back cover:</source>
        <translation>Verwenden Sie die letzte Seite als Rückseite:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="52"/>
        <source>Generate booklet</source>
        <translation>Broschüre generieren</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="68"/>
        <source>Save booklet PDF file</source>
        <translation>Broschüre als PDF-Datei speichern</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/booklet.cpp" line="72"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF-Dateien (*.pdf)</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="34"/>
        <source>Delete pages</source>
        <translation>Seiten löschen</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/delete_pages.cpp" line="54"/>
        <source>Save as…</source>
        <translation>Speichern unter…</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Ändere Mehrseiten-Profil</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="39"/>
        <source>Standard size:</source>
        <translation>Standardgröße:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="40"/>
        <source>Width:</source>
        <translation>Breite:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="41"/>
        <source>Height:</source>
        <translation>Höhe:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>Zentriert</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>Oben</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>Unten</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>Papierformat des Output</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>Eigene Größe:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>Seiten-Layout</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>Zeilen:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>Spalten:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>Abstand:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>Seiten ausrichten</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>Horizontal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>Vertikal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>Ränder</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="34"/>
        <source>Edit page layout</source>
        <translation>Seitenlayout bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="55"/>
        <source>No rotation</source>
        <translation>Keine Drehung</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="62"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="64"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="124"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="211"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="72"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="136"/>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="219"/>
        <source>New custom profile…</source>
        <translation>Neues eigenes Profil…</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="78"/>
        <source>Multipage:</source>
        <translation>Mehrseitig:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="86"/>
        <source>Scale page:</source>
        <translation>Seite skalieren:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/edit_page_layout.cpp" line="94"/>
        <source>Save as…</source>
        <translation>Speichern unter…</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Bearbeite Eigenschaften der PDF-Dateien</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Keine Drehung</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Mehrseiten:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="53"/>
        <source>Output PDF base name:</source>
        <translation>Basisname des Ausgabe-PDFs:</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="49"/>
        <source>Extract to individual PDF files</source>
        <translation>In einzelne PDF-Dateien extrahieren</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="37"/>
        <source>Extract pages</source>
        <translation>Seiten extrahieren</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="62"/>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="84"/>
        <source>Extract…</source>
        <translation>Extrahieren…</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="79"/>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="190"/>
        <source>Extract to single PDF</source>
        <translation>In ein einzelnes PDF extrahieren</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="114"/>
        <source>Select save directory</source>
        <translation>Speicherverzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="../src/single_file_operations/extract_pages.cpp" line="194"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF-Dateien (*.pdf)</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="113"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="80"/>
        <source>Page order:</source>
        <translation>Seitenreihenfolge:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="83"/>
        <source>reverse</source>
        <translation>rückwärts</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="85"/>
        <source>forward</source>
        <translation>vorwärts</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <source>Pages:</source>
        <translation>Seiten:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="129"/>
        <source>Multipage:</source>
        <translation>Mehrseitiges:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="134"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="137"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="140"/>
        <source>Outline entry:</source>
        <translation>Gliederungseintrag:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="57"/>
        <source>Reverse page order:</source>
        <translation>Seitenreihenfolge umkehren:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="71"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="79"/>
        <source>New custom profile…</source>
        <translation>Neues eigenes Profil…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="81"/>
        <source>No rotation</source>
        <translation>Keine Drehung</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="86"/>
        <source>Pages:</source>
        <translation>Seiten:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="88"/>
        <source>Multipage:</source>
        <translation>Mehrseitiges:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="90"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="92"/>
        <source>Outline entry:</source>
        <translation>Gliederungseintrag:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="175"/>
        <source>Add PDF file</source>
        <translation>PDF-Datei hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="187"/>
        <source>Move up</source>
        <translation>Nach oben verschieben</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="192"/>
        <source>Move down</source>
        <translation>Nach unten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>Remove file</source>
        <translation>Datei entfernen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="128"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="248"/>
        <location filename="../src/mainwindow.cpp" line="252"/>
        <source>Generate PDF</source>
        <translation>PDF-Datei erzeugen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="473"/>
        <source>Select the JSON file containing the files list</source>
        <translation>Wählen Sie die JSON-Datei, die die Dateiliste enthält</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="853"/>
        <source>PDF generation error</source>
        <translation>Fehler beim Erzeugen des PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="180"/>
        <source>Select one or more PDF files to open</source>
        <translation>Wähle ein oder mehrere PDF-Dateien zum Öffnen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="165"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <source>Main toolbar</source>
        <translation>Hauptwerkzeugleiste</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="95"/>
        <source>Multiple files</source>
        <translation>Mehrere Dateien</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="96"/>
        <source>Single file</source>
        <translation>Einzelne Datei</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="123"/>
        <source>Multipage profiles…</source>
        <translation>Mehrseiten-Profile…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="133"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="208"/>
        <source>Load files list</source>
        <translation>Dateiliste laden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="213"/>
        <source>Save files list</source>
        <translation>Dateiliste speichern</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="311"/>
        <source>Open PDF file…</source>
        <translation>PDF-Datei öffnen…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <location filename="../src/mainwindow.cpp" line="864"/>
        <location filename="../src/mainwindow.cpp" line="952"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF-Dateien (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="48"/>
        <source>Alternate mix</source>
        <translation>Alternativer Mix</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="475"/>
        <location filename="../src/mainwindow.cpp" line="493"/>
        <source>JSON files (*.json)</source>
        <translation>JSON-Dateien (*.json)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="481"/>
        <source>Error while reading the JSON file!</source>
        <translation>Fehler beim Lesen der JSON-Datei!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="482"/>
        <source>An error occurred while reading the JSON file!</source>
        <translation>Beim Lesen der JSON-Datei ist ein Fehler aufgetreten!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="489"/>
        <source>Select a JSON file</source>
        <translation>Wählen Sie eine JSON-Datei aus</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="602"/>
        <location filename="../src/mainwindow.cpp" line="971"/>
        <source>Error opening file</source>
        <translation>Fehler beim Öffnen der Datei</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <source>Output pages: %1</source>
        <translation>Ausgabe-Seiten: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="950"/>
        <source>Select a PDF file</source>
        <translation>Wählen Sie eine PDF-Datei aus</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1007"/>
        <source>Files saved in %1.</source>
        <translation>Dateien gespeichert in %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>File %1 saved.</source>
        <translation>Datei %1 gespeichert.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="836"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Ausgabeseiten der Datei &lt;b&gt;%1&lt;/b&gt; sind schlecht formatiert. Bitte stellen Sie sicher, dass Sie die folgenden Regeln eingehalten haben:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Intervalle von Seiten müssen geschrieben werden, wobei die erste Seite und die letzte Seite durch einen Bindestrich getrennt sind (z.B. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;Einzelseiten und Intervalle von Seiten müssen durch Leerzeichen, Kommas oder beides getrennt sein (z.B.g. &quot;1, 2, 3, 5-10&quot; oder &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;alle Seiten und Intervalle von Seiten müssen zwischen 1 und der Seitenzahl der PDF-Datei liegen;&lt;/li&gt;&lt;li&gt;nur Zahlen, Leerzeichen, Kommas und Bindestriche können verwendet werden. Alle anderen Zeichen sind nicht erlaubt.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="860"/>
        <source>Save PDF file</source>
        <translation>PDF-Datei abspeichern</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Neues Profil…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="35"/>
        <source>Delete profile</source>
        <translation>Profil löschen</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="43"/>
        <source>Manage multipage profiles</source>
        <translation>Verwalte Mehrseiten-Profile</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="76"/>
        <source>Edit profile</source>
        <translation>Profil bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="122"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="124"/>
        <source>Custom profile</source>
        <translation>Eigenes Profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="183"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="192"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="206"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="184"/>
        <source>Profile name can not be empty.</source>
        <translation>Profilname darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="193"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="207"/>
        <source>Profile name already exists.</source>
        <translation>Der Profilname existiert bereits.</translation>
    </message>
</context>
<context>
    <name>PagesSelector</name>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="44"/>
        <source>Pages:</source>
        <translation>Seiten:</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="45"/>
        <source>Even pages</source>
        <translation>Gerade Seiten</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="46"/>
        <source>Odd pages</source>
        <translation>Ungerade Seiten</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="48"/>
        <source>All pages</source>
        <translation>Alle Seiten</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="108"/>
        <source>&lt;p&gt;Page intervals are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Seitenintervalle sind schlecht formatiert. Bitte stellen Sie sicher, dass Sie die folgenden Regeln eingehalten haben:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Intervalle von Seiten müssen geschrieben werden, wobei die erste Seite und die letzte Seite durch einen Bindestrich getrennt sind (z.B. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;Einzelseiten und Intervalle von Seiten müssen durch Leerzeichen, Kommas oder beides getrennt sein (z.B.g. &quot;1, 2, 3, 5-10&quot; oder &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;alle Seiten und Intervalle von Seiten müssen zwischen 1 und der Seitenzahl der PDF-Datei liegen;&lt;/li&gt;&lt;li&gt;nur Zahlen, Leerzeichen, Kommas und Bindestriche können verwendet werden. Alle anderen Zeichen sind nicht erlaubt.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="124"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>hoch</translation>
    </message>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>quer</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/widgets/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n Seite</numerusform>
            <numerusform>%n Seiten</numerusform>
        </translation>
    </message>
</context>
</TS>
